<?php

if ( !defined( 'MEDIAWIKI' ) ) {
	echo( "This file is an extension to the MediaWiki software and cannot be used standalone.\n" );
	die( 1 );
}

$wgPatchOutputTable = array(
	/* do not show mailto: in mailto links */
    /* '<h2>' => '</div></div></div><div class="block"><h2>', */
    /* '</h2>' => '</h2><div><div>' */
    '<h2>' => '</div><div class="block"><h2>',
    '</h2>' => '</h2>'
);

$wgExtensionCredits['skin'][] = array(
	'path' => __FILE__,
	'name' => 'PatchOutput',
	'description' => 'Allows to patch the HTML representation of a page.',
	'version' => '0.2.2',
	'author' => '[https://www.mediawiki.org/wiki/User:RV1971 RV1971]',
	'url' => 'https://www.mediawiki.org/wiki/Extension:PatchOutput'
);

$wgPatchOutput = new PatchOutput();

$wgHooks['OutputPageBeforeHTML'][] = $wgPatchOutput;

class PatchOutput
{
	public function onOutputPageBeforeHTML( OutputPage &$out, &$text ) {
		global $wgPatchOutputTable;

		$text = strtr( $text, $wgPatchOutputTable );

		#return $out;
	}
}

