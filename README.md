Mediawiki Skin
==============

* Copy the files to $IP/skins/BinaryMatrix/
* Copy PatchOutput/PatchOutput.php to $IP/extensions/PatchOutput/PatchOutput.php
* Modify LocalSettings.php

  Set the skin:

      $wgDefaultSkin = "binarymatrix";

  Add this at the bottom:

      wfLoadSkin( 'BinaryMatrix' );
      require_once "$IP/extensions/PatchOutput/PatchOutput.php";
