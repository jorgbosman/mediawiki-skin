<?php
/* debugging only
function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
} /* */?>

<?php
/*
* Skin file for skin BinaryMatrix.
*
* @file
* @ingroup Skins
*/

/*
* SkinTemplate class for BinaryMatrix skin
* @ingroup Skins
*/

class SkinBinaryMatrix extends SkinTemplate {

    var $skinname = 'binarymatrix', $stylename = 'BinaryMatrix',
        $template = 'BinaryMatrixTemplate', $useHeadElement = true;

    /*
    * @param OutputPage $out
    */
    public function initPage( OutputPage $out ) {
        parent::initPage( $out );
        $out->addModules( 'skins.binarymatrix.js' );
    }

    /*
    * @param $out OutputPage
    */
    function setupSkinUserCss( OutputPage $out ){
        parent::setupSkinUserCss( $out );
        $out->addModuleStyles( array(
            'mediawiki.skinning.interface', 'skins.binarymatrix'
        ) );
    }

}

/*
* BaseTemplate class for BinaryMatrix skin
* @ingroup Skins
*/
class BinaryMatrixTemplate extends BaseTemplate {

    /*
    * Outputs the entire contents of the page
    */
    public function execute() {
        // Suppress warnings to prevent notices about missing indexes in $this->data
        //wfSuppressWarnings();

        $this->html( 'headelement' ); ?>


<nav class="navbar navbar-expand navbar-dark fixed-top topmenu">
  <div class="container-fluid" id="resizable">
    <a class="navbar-brand" href="/">
        <?php echo str_replace('https://', '', str_replace('bosman', '<span style="color: #bf360c">bosman</span>', $this->data['serverurl'])); ?>
    </a>
    <ul class="navbar-nav navbar-right ms-auto mb-2 mb-lg-0">
      <?php
      $first_ul = true;
      foreach ( $this->getSidebar() as $boxName => $box ) {
          // only display toolbox when logged in
          if ($box['header'] <> 'Tools' or $this->data['loggedin']) {
              if ($first_ul == false) { ?>
                  <li class="nav-item dropdown"><a <?php if ($box['header'] == 'Tools') { ?>style="color: #86888a"<?php } ?> class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><?php echo htmlspecialchars( $box['header'] ); ?></a>
                  <?php if ( is_array( $box['content'] ) ) { ?>
                      <ul class="dropdown-menu dropdown-menu-end dropdown-menu-dark" aria-labelledby="navbarDropdown">
                  <?php }
              } ?>
              <?php foreach ( $box['content'] as $key => $item ) {
                  echo $this->makeListItem( $key, $item );
              } 
              if ($first_ul == false and is_array( $box['content'] ) ) { ?>
                  </ul>
              <?php } else {
                  $first_ul = false;
              }
              if (is_array( $box['content']) == false) {
                  echo $box['content'];
              } ?>
              </li>
          <?php }
      }
      if($this->data['loggedin']) { ?>
          <?php foreach ( $this->data['content_navigation'] as $category => $tabs ) {
              if ($category <> 'views' and count($tabs) > 0) { ?>
                  <li class="nav-item dropdown"><a style="color: #86888a" href="#" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><?php echo ucfirst($category) ?></a>
                      <ul class="dropdown-menu dropdown-menu-end dropdown-menu-dark" aria-labelledby="navbarDropdown">
                          <?php foreach ( $tabs as $key => $tab ) { ?>
                          <?php echo $this->makeListItem( $key, $tab ); ?>
                          <?php } ?>
                      </ul>
                  </li>
              <?php }
          }
      } ?>

      <li class="nav-item dropdown">
          <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
              </svg>
          </a>
          <ul class="search-window dropdown-menu dropdown-menu-end dropdown-menu-dark" aria-labelledby="navbarDropdown">
              <li>
                  <form action="<?php $this->text( 'wgScript' ); ?>">
                      <input type='hidden' name="title" value="<?php $this->text( 'searchtitle' ) ?>" />
                      <?php echo $this->makeSearchInput( array( 'id' => 'searchInput' ) ); ?>
                  </form>
              </li>
          </ul>
      </li>

      <?php if($this->data['loggedin']) { ?>
      <li id="pt-logout">
          <a href="/index.php?title=Special:UserLogout" data-mw="interface" title="Log out">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                  <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                  <path fill-rule="eIvenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
              </svg>
          </a>
      </li>
      <?php } else { ?>
     <li id="pt-login">
          <a href="/index.php?title=Special:UserLogin" accesskey="o">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
              </svg>
          </a>
      </li>
      <?php } ?>
    </ul>
  </div>
</nav>
<div style="margin-bottom: 90px"></div>

<div id="mw-js-message" style="display:none;"></div>
<?php if ( $this->data['sitenotice'] ) { ?><div id="siteNotice"><?php $this->html( 'sitenotice' ); ?></div><?php } ?>

<div class="container">
    <div class="sitename"><?php echo strtoupper($this->data[ 'sitename' ]); ?></div>
</div>

<div class="container">
    <div class="firstblock">
        <div class="title" id="firstHeading">
            <div class="titlename"><?php $this->html('title') ?></div>
            <?php if($this->data['loggedin']) { ?>
                <div class="titlemenu">
                <?php foreach ( $this->data['content_navigation'] as $category => $tabs ) {
                        if ($category == 'views' and count($tabs) > 0) { ?>
                            <ul class="nav">
                                <?php foreach ( $tabs as $key => $tab ) {
                                    if ($key == 'edit') { $key='<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16"><path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/></svg>'; }
                                    elseif ($key == 'history') { $key='<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16"><path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/><path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/><path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/></svg>'; }
                                    elseif ($key == 'view') { $key='<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-journal" viewBox="0 0 16 16"><path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/><path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/></svg>'; }
                                    echo '<li class="' . $tab['class'] . '" id="' . $tab['id'] . '"><a href="' . $tab['href'] . '">' . $key . '</a></li>';
                                } ?>
                            </ul>
                        <?php }
                } ?>
                </div>
            <?php } ?>
        </div>
        <?php if ( $this->data['subtitle'] ) { ?>...<?php } ?><?php if ( $this->data['undelete'] ) { ?>...<?php } ?>

        <?php $this->html( 'bodytext' ) ?>
        <?php $this->html( 'dataAfterContent' ); ?>
    </div>
</div>

<?php $this->printTrail(); ?>
    <div style="margin-bottom: 60px"></div>

    <script>
        window.onscroll = function() {
            console.log('x');
            if (document.documentElement.scrollTop > 190) {
                document.getElementById('resizable').classList.add('resized');
            } else {
                document.getElementById('resizable').classList.remove('resized');
            }
        };
    </script>
</body>
</html>
<?php //wfRestoreWarnings(); 
} } ?>

